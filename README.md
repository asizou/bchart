# bchart

## Import updated data

1. [Btc history data src](https://www.cryptodatadownload.com/data/bitstamp/)
   * https://www.cryptodatadownload.com/cdd/Gemini_BTCUSD_1h.csv
1. [Covert csv to json](https://csvjson.com/csv2json)
1. [Edit json online](https://jsoneditoronline.org)
1. [Minify json](https://codebeautify.org/jsonminifier)

Convert hourly json to more compact:
```javascript
function query (data) {
  return data.map(el => {
    var year = parseInt(el.date.substring(0,4)) % 2000;
    var m = parseInt(el.date.substring(5,7));
    var d = parseInt(el.date.substring(8,10));
    var h = parseInt(el.date.substring(11,13));
    var date = year.toString().padStart(2, '0') + m.toString().padStart(2, '0') + d.toString().padStart(2, '0') + h.toString().padStart(2, '0');
    return [parseInt(date), el.open];
  });
}
```

## TODO:
1. remove zero value from hourly data 

## Tools

1. [csvtojson js lib](https://github.com/Keyang/node-csvtojson)